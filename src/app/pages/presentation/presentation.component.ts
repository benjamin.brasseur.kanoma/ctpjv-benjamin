import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import Reveal from 'reveal.js'
import ImgAssets from '../../common/imgAssets'
import AngularHash from "../../reveal/plugins/AngularHash";
import {Code} from "../../common/code/code";
import RevealHighlight from "reveal.js/plugin/highlight/highlight";
import Highlight from "../../reveal/plugins/Highlight";
import {NgbToastModule} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-presentation',
  standalone: true,
  imports: [CommonModule, NgOptimizedImage, NgbToastModule],
  templateUrl: './presentation.component.html',
  styleUrl: './presentation.component.scss',
})
export class PresentationComponent implements OnInit, AfterViewInit {
  imgAssets = ImgAssets;
  /* eslint-disable @typescript-eslint/no-var-requires */
  Characterbody2dspace: Code = require('../../common/code/characterbody2dspace.js').default
  Handlingout: Code = require('../../common/code/handlingout.js').default
  physicsProcessExample: Code = require('../../common/code/physicsprocess.js').default
  processExample: Code = require('../../common/code/process.js').default
  readyExample: Code = require('../../common/code/ready.js').default
  uniqueName: Code = require('../../common/code/uniqueName.js').default
  exportExample: Code = require('../../common/code/export.js').default
  /* eslint-disable @typescript-eslint/no-var-requires */

  @ViewChild('toast') toast!: ElementRef<HTMLElement>;

  ngOnInit() {
    const deck = new Reveal()
    deck.initialize({
      plugins: [AngularHash, RevealHighlight, Highlight],
      embedded: true,
      // @ts-expect-error Bad typing issues but ok based on official documentation
      scrollActivationWidth: null
    })

    window.addEventListener('resize', () => {
      this.displayToast();
    })
  }


  ngAfterViewInit() {
    this.displayToast();
  }


  private displayToast() {
    const width = document.body.clientWidth
    const height = document.body.clientHeight 
    console.log(this.toast)
    console.log(`${width}/${height}`)
    if (width <= height) {
      this.toast.nativeElement.classList.add("show")
      console.log("Vertical")
      return;
    }
    this.toast.nativeElement.classList.remove("show")
  }
}
