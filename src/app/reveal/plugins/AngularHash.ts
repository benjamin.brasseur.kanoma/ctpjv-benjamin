import Reveal from "reveal.js";

export default (): Reveal.Plugin => ({
  id: 'angularHash',
  init: (deck) => {
    if (deck.getConfig().hash) {
      console.error('Angular Hash is an alternative to the hash. Please set hash: false; on your reveal instance.');
      return;
    }

    const url = window.location.href.split('#');
    const baseUrl = url[0];
    let currentSlide: [number, number, number] = [0, 0, 0];
    let currentSlideId: string | null = null;

    deck.on('ready', () => {
      if (url.length > 1) {
        const fragment = url[1].split("_")
        if (fragment.length === 3) {
          const nFragment = fragment.map(f => parseInt(f));
          const nextSlide = [nFragment[0], nFragment[1], nFragment[2]];
          if (currentSlide === nextSlide) {
            return;
          }
          currentSlide = [nFragment[0], nFragment[1], nFragment[2]];
          deck.slide(currentSlide[0], currentSlide[1], currentSlide[2])
        } else if (fragment.length === 1) {
          currentSlideId = url[1];
        }
        if (window.location.href !== baseUrl && currentSlideId === null &&
          (fragment.length !== 3 || currentSlide.every(s => s === 0))) {
          window.location.href = baseUrl;
        }
      }

      deck.on('slidechanged', event => {
        const previousSlide = currentSlide;
        // @ts-expect-error event type definition is missing index and indexv
        currentSlide = [event.indexh, event.indexv, 0]
        // @ts-expect-error event type definition is missing currentSlide
        const slideId = event.currentSlide.id;
        if (slideId) {
          currentSlideId = slideId;
          updateQueryId(baseUrl, slideId);
          return;
        }
        currentSlideId = null;
        if (previousSlide[0] > currentSlide[0] || previousSlide[1] > currentSlide[1]) {
          // @ts-expect-error event type definition is missing currentSlide
          let fragments = event.currentSlide.dataset.fragment
          if (fragments === null || fragments === undefined) {
            fragments = '-1';
          }
          currentSlide[2] = parseInt(fragments) + 1;
        }
        updateQuery(baseUrl, currentSlide);
      });

      deck.on('fragmentshown', () => {
        currentSlide[2]++;
        updateQuery(baseUrl, currentSlide);
      })

      deck.on('fragmenthidden', () => {
        currentSlide[2]--;
        updateQuery(baseUrl, currentSlide);
      })
    })
  }
})

function updateQuery(baseUrl: string, slidePos: [number, number, number]) {
  if (slidePos.every(s => s === 0)) {
    window.history.pushState({}, "", baseUrl);
    return;
  }
  window.history.pushState({}, "", `${baseUrl}#${slidePos.join('_')}`);
}

function updateQueryId(baseUrl: string, slidePos: string) {
  window.history.pushState({}, "", `${baseUrl}#${slidePos}`);
}
