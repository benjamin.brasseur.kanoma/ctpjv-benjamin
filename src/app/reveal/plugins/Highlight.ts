import Reveal from "reveal.js";
import addGdScript from "../../common/addGdScript";
import RevealHighlight from "reveal.js/plugin/highlight/highlight";

export default (): Reveal.Plugin => ({
  id: 'highligh11',
  init: (deck) => {
    if(!deck.hasPlugin(RevealHighlight().id)) {
      console.error("Highlight requires RevealHighlight to be actived to work properly.")
      return;
    }
    // @ts-expect-error getPlugin is missing a proper declaration type and is missing hljs
    const hljs = deck.getPlugin(RevealHighlight().id).hljs;
    deck.on('ready', () => {
      addGdScript(hljs)
      document.querySelectorAll('pre code').forEach(function($) {
        $.className = $.className.replace('no-highlight gdscript', 'hljs language-gdscript');
      });
      hljs.highlightAll()
    })
  }
})

