export default {
  code: `func _physics_process(delta):
\t# Appel la fonction du même nom depuis la classe étendue
\tsuper(delta)
\t# Déplace l'objet et détecte si une collision a eu lieu avec l'objet
\tvar collision = move_and_collide(velocity)
\tif(collision):
\t\ton_collision(collision.get_collider())`
}