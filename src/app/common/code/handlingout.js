export default {
  code: `extends Node

# Permet de déclarer votre script comme une Node
class_name HandlingOutside

var parent: CharacterBody2DSpace
var maxWidth: float
var maxHeight: float

# Permet de définir une marge avant de détecter la sortie de zone
@export var warpMargin: float = 0.01

func _ready():
\tparent = get_parent()
\tvar viewport = get_viewport().get_visible_rect().size
\tmaxWidth = viewport.x
\tmaxHeight = viewport.y

func _process(_delta):
\tvar outOfWay = false
\t# Prépare le changement de position
\tvar newPos = parent.transform.get_origin()
\t
\tif (newPos.x - warpMargin > maxWidth):
\t\toutOfWay = true
\t\tnewPos.x = -warpMargin
\telif (newPos.x + warpMargin < 0):
\t\toutOfWay = true
\t\tnewPos.x = maxWidth + warpMargin

\tif(newPos.y - warpMargin > maxHeight):
\t\toutOfWay = true
\t\tnewPos.y = -warpMargin
\telif (newPos.y + warpMargin < 0):
\t\toutOfWay = true
\t\tnewPos.y = maxHeight + warpMargin

\t# Si on a détecté une sortie de l'objet alors on le déplace
\tif (outOfWay):
\t\tparent.teleport_object(newPos)
`
};