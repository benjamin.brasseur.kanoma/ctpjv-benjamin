export default {
  code: `func _process(delta):
\t# Si mon joueur appuye sur la touche Z
\tif(Input.is_key_pressed(KEY_Z)):
\t\t# Je vais lui dire d'augmenter sa vitesse pour aller vers l'avant
\t\tvar localVelocity = velocity
\t\tlocalVelocity += -transform.y * acceleration * delta
\t\tvelocity = localVelocity`
}
