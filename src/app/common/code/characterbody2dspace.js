export default {
  code: `# Etend le comportement de CharacterBody2D
extends CharacterBody2D

# Expose le code pour qu'il soit accessible au reste du code du jeu
class_name CharacterBody2DSpace

var teleportObject: bool = false
var rotateObject: bool = false
var newPosition: Vector2
var newRotation: float

# Signale que l'objet veut changer sa position
func teleport_object(aimedPosition: Vector2):
\tnewPosition = aimedPosition
\tteleportObject = true

# Signale que l'objet veut tourner
func rotate_object(aimedRotation: float):
\tnewRotation = aimedRotation
\trotateObject = true

# Cette fonction permet de faire des modifications liées à la physique de l'objet
func _physics_process(_delta):
\t#On met à jour la position de l'objet quand la variable teleprotObject est égale à true
\tif(teleportObject):
\t\ttransform.origin = newPosition
\t\tteleportObject = false

# Permet de détruire l'objet
func kill_me():
\tqueue_free()
`
};
