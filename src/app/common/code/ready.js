export default {
  code: `func _ready():
  # On créer une valeur aléatoire pour donner une direction et une vitesse
  var random = Random.new()
  var direction = random.randv_circle(0.1, 1) * 2
  # On donne cette direction à notre objet pour lui permettre de se déplacer
  velocity = direction`
}