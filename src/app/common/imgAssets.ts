export default {
  // Communs
  bigAsteroid:  '../../assets/img/common/bigAsteroide.png',
  mediumAsteroid:  '../../assets/img/common/mediumAsteroide.png',
  smallAsteroid:  '../../assets/img/common/petitAsteroide.png',
  vaisseau:  '../../assets/img/common/Vaisseau.png',
  bullet:  '../../assets/img/common/bullet.png',

  // Intro
  intro: '../../assets/img/intro/ctpjv-intro.gif',
  asteroidGame: '../../assets/img/intro/asteroidGame.png',
  manette:  '../../assets/img/intro/manette.png',
  souris:  '../../assets/img/intro/souris.png',
  clavier:  '../../assets/img/intro/clavier.png',
  godotDll: '../../assets/img/intro/godot-dll.png',
  godotStart: '../../assets/img/intro/godot-start.png',
  godotNew: '../../assets/img/intro/godot-new.png',
  godotView: '../../assets/img/intro/godot-view.png',
  // Moteurs
  godot: '../../assets/img/intro/moteurs/Godot.png',
  gamemaker: '../../assets/img/intro/moteurs/GameMaker.png',
  phaser: '../../assets/img/intro/moteurs/Phaser.png',
  unity: '../../assets/img/intro/moteurs/Unity.png',
  unreal: '../../assets/img/intro/moteurs/UnrealEngine.png',
  // Conclusion
  // Jams
  ggj: '../../assets/img/conclusion/jams/ggj.png',
  gmtk: '../../assets/img/conclusion/jams/gmtk.png',
  ludum: '../../assets/img/conclusion/jams/ludum.png',
  // Documentation
  characterbody2DCollision: '../../assets/img/doc/characterbody2DCollision.png',
  exportExample: '../../assets/img/doc/export-example.png',
  uniqueNameExample: '../../assets/img/doc/uniquename-example.png',

  // ?
  arrow: '../../assets/img/arrow.png',

}
